# README #

This project is meant to connect an array of ds18b20 temperature sensors to an Arduino board with an LCD and to display the temperature values.

### Hardware requirements ###

* [Arduino Mega 2560](ArduinoBoardMega2560) - or compatible Arduino board
* [Arduino LCD KeyPad Shield (SKU: DFR0009)](http://www.dfrobot.com/wiki/index.php?title=Arduino_LCD_KeyPad_Shield_(SKU:_DFR0009))
* [DS18B20](https://www.maximintegrated.com/en/products/analog/sensors-and-sensor-interface/DS18B20.html) - one or more
* 4k7 Ohm Resistor
* Optional: breadboard and connection wires

### How do I get set up? ###

* *Hardware*      - Follow the documentation found in */Manual* folder in order to set up the hardware. Among other things, there are a few jpeg schematics which depict the one-wire protocol and the parasite power mode of the ds18b20 chip
* *Configuration* - For the first use set up the following constants with your appropriate values: ONE_WIRE_BUS, TEMPERATURE_PRECISION. These are found in the main ArduinoTemperatureStation.ino file
* *Dependencies*  - <OneWire.h>, <DallasTemperature.h>, <LiquidCrystal.h>. Found in the */Library* folder

### Improvement road-map ###

* Support for larger LCD Display(s)
* Support for an Ethernet or Wifi Shield which will post the readings to an API
* Support for a Wifi module which will send the readings to a second module to display the values at a distance from the TemperatureStation

### Contribution guidelines ###

* Code review is welcomed
* Feature suggestions

### Who do I talk to for information? ###

* Iosif <dev@iosifv.com>
