// Prints a device's address in a readable format
void printTempSensorAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++) {
    // zero pad the address if necessary
    if (deviceAddress[i] < 16) {
      Serial.print("0");
    }
    Serial.print(deviceAddress[i], HEX);
  }
}

// Prints number of sensor devices
void printTempSensorDevicesFound(int deviceCount)
{
  Serial.print("Found ");
  Serial.print(deviceCount, DEC);
  Serial.println(" devices.");
}

// Prints if parasite power state: ON/OFF
void printTempSensorParasiteMode(bool parasiteMode)
{
  // report parasite power requirements
  Serial.print("Parasite power is: ");
  if (parasiteMode) {
    Serial.println("ON");
  } else {
    Serial.println("OFF");
  }
}

// Prints initial information about a sensor: Device number, Address and Resolution
void printTempSensorSetupInfo(DeviceAddress device, int deviceCount)
{
  Serial.print("(device ");      Serial.print(deviceCount, DEC);    Serial.print(") ");
  Serial.print("Address: ");     printTempSensorAddress(device);
  Serial.print(" Resolution: "); Serial.print(sensors.getResolution(device));
  Serial.println();
}

// Prints the array of devices in a nice key->value pairs (Device number, Temperature)
void printTempSensorRuntimeInfo(DeviceAddress myThermometer[], int myDeviceCount)
{
  for (int i; i < myDeviceCount; i++) {
    Serial.print("t(");
    Serial.print(i);
    Serial.print(")= ");
    float tempC = sensors.getTempC(myThermometer[i]);
    Serial.print(tempC);
    Serial.print(" | ");
  }
  Serial.println();
}

