// read the buttons
int read_LCD_buttons()
{
  // read the value from the sensor
  adc_key_in = analogRead(0);
  // Buttons from my board, when read are centered at these values:
  //  RIGHT  = 0 
  //  UP     = 97
  //  DOWN   = 252
  //  LEFT   = 403
  //  SELECT = 636
  //  NONE   = 1023
  
  // This the 1st option for speed reasons since it will be the most likely result
  if (adc_key_in > 1000) return btnNONE; 

  // For V1.1 us this threshold
  if (adc_key_in < 50)   return btnRIGHT;
  if (adc_key_in < 150)  return btnUP;
  if (adc_key_in < 350)  return btnDOWN;
  if (adc_key_in < 500)  return btnLEFT;
  if (adc_key_in < 750)  return btnSELECT;

  // For V1.0 comment the other threshold and use the one below:
  /*
    if (adc_key_in < 50)   return btnRIGHT;
    if (adc_key_in < 195)  return btnUP;
    if (adc_key_in < 380)  return btnDOWN;
    if (adc_key_in < 555)  return btnLEFT;
    if (adc_key_in < 790)  return btnSELECT;
  */

  // when all others fail, return this
  return btnNONE;
}

// Test function which prints the milliseconds passed since start-up
void displayTestMills(LiquidCrystal lcd)
{
  lcd.setCursor(9, 1);      // move cursor to second line "1" and 9 spaces over
  lcd.print(millis() / 10); // display seconds elapsed since power-up
}

// Test function which prints the name of the pressed button
void displayTestBtnPressed(LiquidCrystal lcd)
{
  // move to the begining of the second line and read the buttons
  lcd.setCursor(0, 1);
  lcdBtnPressed = read_LCD_buttons();

  // depending on which button was pushed, a relevant text is shown
  switch (lcdBtnPressed) {
    case btnRIGHT:  lcd.print("RIGHT "); break;
    case btnLEFT:   lcd.print("LEFT  "); break;
    case btnUP:     lcd.print("UP    "); break;
    case btnDOWN:   lcd.print("DOWN  "); break;
    case btnSELECT: lcd.print("SELECT"); break;
    case btnNONE:   lcd.print("NONE  "); break;
  }
}

// Prints a device's address in a readable format
void displayTempSensorAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++) {
    // zero pad the address if necessary
    if (deviceAddress[i] < 16) {
      lcd.print("0");
    }
    lcd.print(deviceAddress[i], HEX);
  }
}

// Prints initial information about a sensor: Device number, Address and Resolution
void displayTempSensorSetupInfo(DeviceAddress device, int deviceCount)
{
  lcd.setCursor(0, 0);
  lcd.print("S");      lcd.print(deviceCount, DEC);
  lcd.print(" Res: "); lcd.print(sensors.getResolution(device));
  lcd.setCursor(0, 1);
  displayTempSensorAddress(device);
  delay(500);
}

// This prints the value of all readings to the lcd in the folowing format:
// t0  t1  t2  t3
// t4  t5  t6  t7
// myDeviceCount should be lower than 5
void displaySensorReadingsOverview(LiquidCrystal lcd, DeviceAddress myThermometer[], int myDeviceCount)
{
  int x, y, temperatureReading;
  int lengthOfPrint = 4;
  int printsPerRow  = 4;
  for (int i = 0; i < myDeviceCount; i++) {
    x = (i * lengthOfPrint) % (lengthOfPrint * printsPerRow);
    y = trunc(i / printsPerRow);
    // asigning the reading to a INT variable automatically truncates the value. 
    temperatureReading = sensors.getTempC(myThermometer[i]);
    lcd.setCursor(x, y);
    lcd.print(temperatureReading);
  }
}

// This prints the value of all readings to the lcd in the folowing format:
// 0:t0.t0  1:t1.t1 
// 2:t2.t2  3:t3.t3
// myDeviceCount should be lower than 5
void displaySensorReadingsDetail_1(LiquidCrystal lcd, DeviceAddress myThermometer[], int myDeviceCount)
{
  int x, y, temperatureReading;
  int lengthOfPrint = 9;
  int printsPerRow  = 2;

  for (int i = 0; i < 4; i++) {
    x = (i * lengthOfPrint) % (lengthOfPrint * printsPerRow);
    y = trunc(i / printsPerRow);
    lcd.setCursor(x, y);
    lcd.print(i);
    lcd.print(':');
    lcd.print(sensors.getTempC(myThermometer[i]));
  }
}

// This prints the value of all readings to the lcd in the folowing format:
// 4:t4.t4  5:t5.t5
// 6:t6.t6  7:t7.t7
// myDeviceCount should be lower than 5
void displaySensorReadingsDetail_2(LiquidCrystal lcd, DeviceAddress myThermometer[], int myDeviceCount)
{
  int x, y, temperatureReading;
  int lengthOfPrint = 9;
  int printsPerRow  = 2;
  
  for (int i = 0; i < 4; i++) {
    x = (i * lengthOfPrint) % (lengthOfPrint * printsPerRow);
    y = trunc(i / printsPerRow);
    lcd.setCursor(x, y);
    lcd.print(i+4);
    lcd.print(':');
    lcd.print(sensors.getTempC(myThermometer[i+4]));
  }
}


