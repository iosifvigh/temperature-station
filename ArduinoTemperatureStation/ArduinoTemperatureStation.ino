#include <OneWire.h>
#include <DallasTemperature.h>
#include <LiquidCrystal.h>

#define ONE_WIRE_BUS 24          // Data wire 
#define TEMPERATURE_PRECISION 11 // Default sensor resolution. Available values: 9,10,11,12

// define some values used by the LCD panel and buttons
int lcdBtnPressed = 0;
int adc_key_in    = 0;
#define btnRIGHT  0
#define btnUP     1
#define btnDOWN   2
#define btnLEFT   3
#define btnSELECT 4
#define btnNONE   5

#define DISPLAY_STATE_OVERVIEW 0
#define DISPLAY_STATE_DETAIL_1 1
#define DISPLAY_STATE_DETAIL_2 2
int currentDisplayState  = DISPLAY_STATE_OVERVIEW;
int previousDisplayState = -1;

LiquidCrystal     lcd(8, 9, 4, 5, 6, 7); // select the pins used on the LCD panel
OneWire           oneWire(ONE_WIRE_BUS); // Setup a oneWire instance to communicate with any OneWire devices
DallasTemperature sensors(&oneWire);     // Pass oneWire reference to Dallas Temperature
DeviceAddress     myThermometer[10];     // arrays to hold device addresses
int               myDeviceCount;


///***************************** INITIAL SETUP
void setup(void)
{
  Serial.begin(9600);  // start up the serial port
  sensors.begin();     // Start up the DallasTemperature library
  lcd.begin(16, 2);    // Start up the LCD library

  myDeviceCount = sensors.getDeviceCount();   // locate devices on the bus
  printTempSensorDevicesFound(myDeviceCount); // printFountDevices
  printTempSensorParasiteMode(sensors.isParasitePowerMode());

  // Loop through all found sensors
  for (int i; i < myDeviceCount; i++) {
    if (!sensors.getAddress(myThermometer[i], i)) {
      Serial.print("Unable to find address for device ");
      Serial.println(i, DEC);
      continue;
    }
    sensors.setResolution(myThermometer[i], TEMPERATURE_PRECISION);
    printTempSensorSetupInfo(myThermometer[i], i);
    displayTempSensorSetupInfo(myThermometer[i], i);
  }
}

///***************************** MAIN LOOP
void loop(void)
{
  sensors.requestTemperatures();
  printTempSensorRuntimeInfo(myThermometer, myDeviceCount);

  // Change the state of the machine according to which button is pressed
  lcdBtnPressed = read_LCD_buttons();
  switch (lcdBtnPressed) {
    case btnRIGHT:
      break;
    case btnLEFT:
      currentDisplayState =  DISPLAY_STATE_OVERVIEW;
      break;
    case btnUP:
      currentDisplayState =  DISPLAY_STATE_DETAIL_1;
      break;
    case btnDOWN:
      currentDisplayState =  DISPLAY_STATE_DETAIL_2;
      break;
    case btnSELECT:
      currentDisplayState =  DISPLAY_STATE_OVERVIEW;
      break;
    case btnNONE:
      break;
  }

  // Reason for this is so that the LCD is only cleared when a state is changed.
  // No need to clear the LCD in the same state because values will be overwritten anyway.
  if (previousDisplayState != currentDisplayState) {
    previousDisplayState = currentDisplayState;
    lcd.clear();
  }

  // Execute action according to machine state
  switch (currentDisplayState) {
    case DISPLAY_STATE_OVERVIEW:
      displaySensorReadingsOverview(lcd, myThermometer, myDeviceCount);
      break;
    case DISPLAY_STATE_DETAIL_1:
      displaySensorReadingsDetail_1(lcd, myThermometer, myDeviceCount);
      break;
    case DISPLAY_STATE_DETAIL_2:
      displaySensorReadingsDetail_2(lcd, myThermometer, myDeviceCount);
      break;
  }
}


